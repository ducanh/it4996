package ducanh.wavfile2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import ducanh.wavfile2.configs.WavFile2Config;
import ducanh.wavfile2.trackview.TrackViewState;
import ducanh.wavfile2.trackview.TrackViewStateView;
import ducanh.wavfile2.trackview.WavTrackView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FileInfo extends Activity {

	TextView txtFileInfo;
	String fileName;
	Button btnPlay;
	LinearLayout drawFile;

	WavTrackView drawView;
	TrackViewState drawViewState;

	float startPoint;
	float endPoint;
	int startSample, endSample;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.file_info);
		btnPlay = (Button) findViewById(R.id.btnPlay);

		drawFile = (LinearLayout) findViewById(R.id.drawFile);
		drawFile.setBackgroundColor(Color.BLACK);

		String fileName = getIntent().getStringExtra("fileName");
		txtFileInfo = (TextView) findViewById(R.id.txtFileInfo);
		this.fileName = fileName;

		drawView = new WavTrackView(this);
		drawViewState = new TrackViewStateView(drawView, fileName);
		drawView.setState(drawViewState);
		drawFile.addView(drawView);

		btnPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				drawView.play();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.file_info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.delete_file) {
			File file = new File(this.fileName);
			file.delete();
			Intent listFile = new Intent(getApplicationContext(),
					ListFile.class);
			startActivity(listFile);
			finish();
		} else if (id == R.id.view_ttq) {
			Intent viewTTQ = new Intent(getApplicationContext(), ViewTTQ.class);
			viewTTQ.putExtra("fileName", this.fileName);
			startActivity(viewTTQ);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
