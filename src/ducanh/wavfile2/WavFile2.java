package ducanh.wavfile2;

import ducanh.wavfile2.files.WavRecorder;
import ducanh.wavfile2.trackview.TrackViewStateRecording;
import ducanh.wavfile2.trackview.TrackViewStateView;
import ducanh.wavfile2.trackview.WavTrackView;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WavFile2 extends ActionBarActivity{

	TextView txtMsg;
	Button btnStart;
	Button btnStop;
	LinearLayout wavTrackViewContainer;
	WavTrackView wavTrackView;
	
	WavRecorder wavRecorder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wav_file2);
		wavTrackViewContainer = (LinearLayout) findViewById(R.id.wavTrackViewContainer);
		wavTrackViewContainer.setBackgroundColor(Color.BLACK);
		
		wavTrackView = new WavTrackView(this);
		wavTrackViewContainer.addView(wavTrackView);
		
		wavRecorder = new WavRecorder();
		wavRecorder.setOnRecordingListener(wavTrackView);
		
		txtMsg = (TextView) findViewById(R.id.txtMsg);

		btnStart = (Button) findViewById(R.id.btnStart);
		btnStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				wavRecorder.startRecording();
				enableButtons(wavRecorder.isRecording());
				wavTrackView.setState(new TrackViewStateRecording(wavTrackView));
			}
		});

		btnStop = (Button) findViewById(R.id.btnStop);
		btnStop.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				wavRecorder.stopRecording();
				enableButtons(wavRecorder.isRecording());
				wavTrackView.setState(new TrackViewStateView(wavTrackView, wavRecorder.getNewFileName()));
			}
		});
	}
	
	private void enableButtons(boolean isRecording) {
		btnStart.setEnabled(!isRecording);
		btnStop.setEnabled(isRecording);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.wav_file2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.file_browser) {
			Intent listFile = new Intent(getApplicationContext(), ListFile.class);
			startActivity(listFile);
			return true;
		}
		if (id == R.id.file_play) {
			wavTrackView.play();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
