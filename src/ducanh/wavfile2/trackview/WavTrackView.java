package ducanh.wavfile2.trackview;

import java.util.ArrayList;
import java.util.List;

import ducanh.wavfile2.files.WavRecorder.OnRecordingListener;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class WavTrackView extends View implements OnRecordingListener {

	Paint paint = new Paint();
	int drawColor, focusColor;

	float x0, y0, maxY, stepX;
	float unitX, unitY, oUnitY;
	PointF lastPoint;

	int sampleRate;
	int stepViewMs;
	int currDrawSample = 0;
	List<Integer> data;

	Canvas mCanvas;
	Bitmap drawBitmap = null;

	TrackViewState trackViewState;

	public WavTrackView(Context context) {
		super(context);
		drawColor = Color.parseColor("#35d7ea");
		focusColor = Color.parseColor("#ffd146");
		lastPoint = new PointF(0.0f, 0.0f);
		trackViewState = new TrackViewStateNull(this);
		stepViewMs = 0;

		x0 = 0;
		unitX = 1.0f;
		oUnitY = unitY = 0.005f;
		stepX = 0.0f;
		// this.data = data;
		// this.sampleRate = samplerate;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		populateMetrics();
		setDrawingCacheEnabled(true);
		paint.setColor(drawColor);
		this.trackViewState.onDrawing(canvas);
	}

	private void populateMetrics() {
		y0 = this.getHeight() / 2;
		maxY = this.getHeight() / 3 / unitY;
		if (drawBitmap == null) {
			drawBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(),
					Bitmap.Config.ARGB_8888);
			mCanvas = new Canvas(drawBitmap);
		}
	}

	public void drawLine(PointF point1, PointF point2, boolean fixedPos) {
		float x = fixedPos ? 0 : x0;
		point1.y = point1.y > maxY ? maxY : point1.y;
		point1.y = point1.y < -maxY ? -maxY : point1.y;
		point2.y = point2.y > maxY ? maxY : point2.y;
		point2.y = point2.y < -maxY ? -maxY : point2.y;
		mCanvas.drawLine(x + point1.x * unitX, y0 - point1.y * unitY, x
				+ point2.x * unitX, y0 - point2.y * unitY, paint);
	}

	public void drawLines(List<PointF> listPoints) {
		int i;
		if (listPoints.size() >= 2) {
			for (i = 0; i < listPoints.size() - 1; i++) {
				this.drawLine(listPoints.get(i), listPoints.get(i + 1), false);
				lastPoint = listPoints.get(i + 1);
				if (this.sampleRate != 0 && this.stepViewMs != 0 && this.currDrawSample % this.stepViewMs == 0) {
					PointF point = new PointF(listPoints.get(i).x, -maxY - 20);
					this.drawText(String.format("%.1f", (float)this.currDrawSample / this.sampleRate) + "", point, false); // s

				}
				this.currDrawSample++;
			}
		}
	}

	public void drawRect(PointF leftTop, PointF rightBottom, boolean fixedPos) {
		float x = fixedPos ? 0 : x0;
		mCanvas.drawRect(x + leftTop.x * unitX, y0 - leftTop.y * unitY, x
				+ rightBottom.x * unitX, y0 - rightBottom.y * unitY, paint);
	}

	public void drawText(String text, PointF point1, boolean fixedPos) {
		float x = fixedPos ? 0 : x0;
		mCanvas.drawText(text, x + point1.x * unitX, y0 - point1.y * unitY,
				paint);
	}

	public void drawData(List<Integer> data) {
		ArrayList<PointF> listPoints = this.getListPoints(data);
		this.drawLines(listPoints);
	}

	private ArrayList<PointF> getListPoints(List<Integer> data) {
		ArrayList<PointF> listPoints = new ArrayList<PointF>();
		if (data != null && data.size() > 0) {
			int numberSample = data.size();
			// Bat dau ve tu diem cuoi cung
			listPoints.add(lastPoint);
			float x = lastPoint.x + stepX;
			for (int i = 0; i < numberSample; i++) {
				float y = (float) data.get(i);
				listPoints.add(new PointF(x, y));
				x += stepX;
			}
		}
		return listPoints;
	}

	public float getZoomScale() {
		return unitY / oUnitY;
	}

	@Override
	public void onRecording(byte[] data) {
		// TODO Auto-generated method stub
		this.trackViewState.onRecording(data);
	}

	public void setState(TrackViewState state) {
		this.trackViewState = state;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		this.trackViewState.onTouchEvent(event);
		return true;
	}

	public void play() {
		this.trackViewState.play();
	}
}
