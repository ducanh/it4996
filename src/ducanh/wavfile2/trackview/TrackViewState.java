package ducanh.wavfile2.trackview;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.view.MotionEvent;

public abstract class TrackViewState {
	WavTrackView view;
	public TrackViewState(WavTrackView view){
		this.view = view;
		view.x0 = 0;
		view.currDrawSample = 0;
		view.drawBitmap = null;
		view.invalidate();
	}
	public abstract void onDrawing(Canvas canvas);
	public abstract void onRecording(byte[] data);
	public abstract void onTouchEvent(MotionEvent event);
	public abstract void play();
}
