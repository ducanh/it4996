package ducanh.wavfile2.trackview;

import java.util.ArrayList;
import java.util.List;

import ducanh.wavfile2.configs.WavFile2Config;
import ducanh.wavfile2.files.FileUtils;

import android.R.integer;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.view.MotionEvent;

public class TrackViewStateRecording extends TrackViewState {

	public TrackViewStateRecording(WavTrackView view) {
		super(view);
		view.lastPoint = new PointF(0.0f, 0.0f);
		view.stepViewMs = WavFile2Config.RECORDER_SAMPLERATE / 2;
		view.sampleRate = WavFile2Config.RECORDER_SAMPLERATE;
	}

	@Override
	public void onDrawing(Canvas canvas) {
		// TODO Auto-generated method stub
		initDrawBitmap();
		view.stepX = 0.005f;
		canvas.drawBitmap(view.drawBitmap, 0, 0, view.paint);
		view.drawLine(new PointF(0.0f, 0.0f), new PointF(view.getWidth(), 0.0f), true);
	}

	private void initDrawBitmap() {
		if (view.drawBitmap == null) {
			view.drawBitmap = Bitmap.createBitmap(view.getWidth(),
					view.getHeight(), Bitmap.Config.ARGB_8888);
			view.mCanvas = new Canvas(view.drawBitmap);
		}
	}
	
	@Override
	public void onRecording(byte[] data) {
		// TODO Auto-generated method stub
		int dataLen = data.length;
		// PHAN NAY DOC CO DINH ==========
		List<Integer> dataInt = new ArrayList<Integer>();
		for (int i = 0; i < dataLen; i += 2) {
			int y = (short) (((data[i + 1] & 0xff) << 8) | ((data[i] & 0xff)));
			dataInt.add(y);
		}
		view.drawData(dataInt);
		
		float check =view.lastPoint.x - view.getWidth() + view.x0;
		if(check > 0){
			view.lastPoint.x = 0;
			view.drawBitmap = null;
		}
		view.invalidate();
	}

	@Override
	public void onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		
	}

}
