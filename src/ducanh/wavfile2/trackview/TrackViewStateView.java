package ducanh.wavfile2.trackview;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import ducanh.wavfile2.configs.WavFile2Config;
import ducanh.wavfile2.files.WavReader;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.Toast;

public class TrackViewStateView extends TrackViewState {

	WavReader reader;
	int sampleRate, sampleSize, numberSample, numberChanels;

	String fileName;

	List<Integer> drawData;
	List<byte[]> playData;

	private ScaleGestureDetector mScaleDetector;
	private float mScaleFactorX = 1.f;
	private float mScaleFactorY = 1.f;

	float marker1X = -100, marker2X = -100;
	float tmpMarker1X = -100, tmpMarker2X = -100;
	float tmpMarker1Y = 0, tmpMarker2Y = 0;
	float dX = 0;

	float pointerX = -100;

	private Handler mHandler;
	private Thread playThread;

	boolean isPlaying = false;

	public TrackViewStateView(final WavTrackView view, String newFileName) {
		super(view);
		// TODO Auto-generated constructor stub
		this.fileName = newFileName;
		this.readFileInfo(newFileName);
		mScaleDetector = new ScaleGestureDetector(view.getContext(),
				new ScaleListener());
		view.sampleRate = this.sampleRate;
		view.stepViewMs = this.numberSample / 10;
		view.stepX = 0.0f;

		mHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				// làm gì đó với msg, có thể lấy dữ liệu bằng msg.getData()
				pointerX = msg.getData().getFloat("pointer_X");
				if (msg.getData().containsKey("xO")) {
					view.x0 = msg.getData().getFloat("x0");
				}
				if (msg.getData().containsKey("marker1X")) {
					marker1X = msg.getData().getFloat("marker1X");
				}
				if (msg.getData().containsKey("marker2X")) {
					marker2X = msg.getData().getFloat("marker2X");
				}
				// if(pointerX ==-100){
				// view.x0 = 0; pointerX = 0;
				// }
				// else if(pointerX - view.getWidth() + view.x0 > 0){
				// dX = -view.getWidth();
				// }

				view.drawBitmap = null;
				view.invalidate();
			}
		};
	}

	@Override
	public void onDrawing(Canvas canvas) {
		// TODO Auto-generated method stub
		initDrawBitmap();
		view.x0 += dX;
		if (view.x0 > 0) {
			view.x0 = 0;
		}
		marker1X += dX;
		marker2X += dX;

		view.unitY *= mScaleFactorY;
		view.stepX *= mScaleFactorX;
		view.stepViewMs = Math.round(view.stepViewMs / mScaleFactorX);
		view.lastPoint.x = view.lastPoint.y = 0.0f;
		view.currDrawSample = 0;
		view.drawData(drawData);
		canvas.drawBitmap(view.drawBitmap, 0, 0, view.paint);
		view.drawLine(new PointF(0.0f, view.maxY), new PointF(view.getWidth(),
				view.maxY), true);
		view.drawLine(new PointF(0.0f, 0.0f),
				new PointF(view.getWidth(), 0.0f), true);
		view.drawLine(new PointF(0.0f, -view.maxY), new PointF(view.getWidth(),
				-view.maxY), true);

		view.drawText("x0: " + view.x0, new PointF(0.0f, view.maxY + 1), true);
		view.drawText("Ponit X: " + pointerX,
				new PointF(100.0f, view.maxY + 1), true);
		view.drawText("Marker 1X: " + marker1X, new PointF(200.0f,
				view.maxY + 1), true);
		view.drawText("Marker 2X: " + marker2X, new PointF(300.0f,
				view.maxY + 1), true);
		drawMarker();
		drawPointer();
	}

	private boolean initMarker() {
		if (marker1X == -100 || marker2X == -100) {
			return false;
		}
		return true;
	}

	private boolean isVaildPointer() {
		return this.pointerX == -100 ? false : true;
	}

	private void drawMarker() {
		if (initMarker()) {
			view.paint.setColor(Color.YELLOW);
			view.paint.setAlpha(70);
			float markerY = view.maxY;
			view.drawRect(new PointF(marker1X, markerY), new PointF(marker2X,
					-markerY), true);
		}

	}

	private void drawPointer() {
		if (isVaildPointer()) {
			view.paint.setColor(Color.WHITE);
			float pointerY = view.maxY;
			view.drawLine(new PointF(pointerX, pointerY), new PointF(pointerX,
					-pointerY), false);
		}
	}

	private void initDrawBitmap() {
		if (view.drawBitmap == null) {
			view.drawBitmap = Bitmap.createBitmap(view.getWidth(),
					view.getHeight(), Bitmap.Config.ARGB_8888);
			view.mCanvas = new Canvas(view.drawBitmap);
		}
		if (view.stepX == 0.0f) {
			view.stepX = (float) view.getWidth() / this.numberSample;
		}
	}

	protected void readFileInfo(String fileName) {
		FileInputStream in = null;
		try {
			in = new FileInputStream(fileName);
			reader = new WavReader(in);
			this.sampleSize = reader.getSampleSize();
			this.sampleRate = reader.getSampleRate();
			this.numberSample = reader.getNumberSamples();
			this.numberChanels = reader.getNumberChanels();
			this.drawData = reader.getDrawData(0, 0);
			this.playData = reader.getPlayData();
			in.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onRecording(byte[] data) {
		// TODO Auto-generated method stub

	}

	private class ScaleListener extends
			ScaleGestureDetector.SimpleOnScaleGestureListener {

		@Override
		public void onScaleEnd(ScaleGestureDetector detector) {
			// TODO Auto-generated method stub
			mScaleFactorX = mScaleFactorY = 1.0f;
			super.onScaleEnd(detector);
		}

		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			mScaleFactorX = detector.getCurrentSpanX()
					/ detector.getPreviousSpanX();
			mScaleFactorY = detector.getCurrentSpanY()
					/ detector.getPreviousSpanY();
			// Don't let the object get too small or too large.
			mScaleFactorX = Math.max(0.8f, Math.min(mScaleFactorX, 4.0f));
			mScaleFactorY = Math.max(0.8f, Math.min(mScaleFactorY, 4.0f));
			dX = 0;
			view.drawBitmap = null;
			view.invalidate();
			return true;
		}
	}

	@Override
	public void onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		mScaleDetector.onTouchEvent(event);
		// Toast.makeText(view.getContext(), mScaleDetector.getScaleFactor() +
		// "",
		// Toast.LENGTH_SHORT).show();
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			tmpMarker1X = event.getX();
			tmpMarker1Y = event.getY();
			break;
		case MotionEvent.ACTION_MOVE:

			break;
		case MotionEvent.ACTION_UP:
			// pointerX = -100;

			tmpMarker2X = event.getX();
			tmpMarker2Y = event.getY();

			float boundMove = view.y0 - view.maxY * view.unitY;
			float boundSelect = view.y0 + view.maxY * view.unitY;

			dX = 0;
			boolean turnLeft = true;
			float min,
			max;
			if (tmpMarker1X < tmpMarker2X) {
				turnLeft = false;
				min = tmpMarker1X;
				max = tmpMarker2X;
			} else {
				max = tmpMarker1X;
				min = tmpMarker2X;
			}

			if (tmpMarker1Y < boundMove && tmpMarker2Y < boundMove) {
				dX = tmpMarker2X - tmpMarker1X;
				view.drawBitmap = null;
				view.invalidate();
			}

			if (tmpMarker1Y > boundSelect && tmpMarker2Y > boundSelect) {
				boolean updateMark1X = marker1X < max && marker1X > min;
				boolean updateMark2X = marker2X < max && marker2X > min;

				if (min == max) {
					marker1X = marker2X = -100;
				} else if (updateMark1X && !updateMark2X) {
					marker1X = turnLeft ? min : max;
				} else if (!updateMark1X && updateMark2X) {
					marker2X = turnLeft ? min : max;
				} else {
					marker1X = min;
					marker2X = max;
				}
				view.drawBitmap = null;
				view.invalidate();
			}

			break;

		}
	}

	private int getSampleNoClick(float x) {
		if (x == 0f) {
			return 1;
		}

		float tmp = x - view.x0;
		if (tmp < 0) {
			return 0;
		} else {
			return Math.round(tmp / view.stepX);
		}
	}

	public void play() {
		isPlaying = true;
		playThread = null;
		playThread = new Thread(new Runnable() {
			@Override
			public void run() {
				_play();
			}
		}, "playThread");
		playThread.start();
	}

	private void _play() {
		int startSamplePlay = 0;
		int endSamplePlay = 0;
		int numberSamplePlay = 0;
		if (initMarker()) {
			startSamplePlay = getSampleNoClick(marker1X);
			endSamplePlay = getSampleNoClick(marker2X);

			if (startSamplePlay > this.numberSample) {
				startSamplePlay = endSamplePlay = 0;
			}

			if (endSamplePlay > this.numberSample) {
				endSamplePlay = this.numberSample;
			}

			numberSamplePlay = endSamplePlay - startSamplePlay;
		}

		// Toast.makeText(view.getContext(),
		// getSampleNoClick(marker1X)+" : "+getSampleNoClick(marker2X),
		// Toast.LENGTH_SHORT).show();

		int i = 0;
		int config_chanel = AudioFormat.CHANNEL_CONFIGURATION_DEFAULT;
		if (this.numberChanels == 1) {
			config_chanel = AudioFormat.CHANNEL_CONFIGURATION_MONO;
		} else if (this.numberChanels == 2) {
			config_chanel = AudioFormat.CHANNEL_CONFIGURATION_STEREO;
		}

		int bufferSize = AudioRecord.getMinBufferSize(this.sampleRate,
				config_chanel, WavFile2Config.RECORDER_AUDIO_ENCODING);

		AudioTrack at = new AudioTrack(AudioManager.STREAM_MUSIC,
				this.sampleRate, config_chanel,
				WavFile2Config.RECORDER_AUDIO_ENCODING, bufferSize,
				AudioTrack.MODE_STREAM);

		at.play();
		try {
			InputStream is = new FileInputStream(fileName);

			is.skip(startSamplePlay * WavFile2Config.RECORDER_BITSPERSAMPLE / 8);

			// Play toan bo
			if (numberSamplePlay == 0) {
				Message msg = mHandler.obtainMessage();
				Bundle msgData = new Bundle();
				msgData.putFloat("x0", 0);
				msgData.putFloat("marker1X", -100);
				msgData.putFloat("marker2X", -100);
				msg.setData(msgData);
				mHandler.sendMessage(msg);

				byte[] music = new byte[512];
				int read = 0;
				while ((i = is.read(music)) != -1) {
					// Ve con tro chay
					float pointer_X = read * view.stepX / this.sampleSize;

					msg = mHandler.obtainMessage();
					msgData = new Bundle();
					msgData.putFloat("pointer_X", pointer_X);
					msg.setData(msgData);
					mHandler.sendMessage(msg);
					at.write(music, 0, i);

					read += i;
				}

			} else if (numberSamplePlay >= 512 / this.sampleSize) {
				byte[] music = new byte[512];
				int read = 0;
				while ((i = is.read(music)) != -1
						&& read <= numberSamplePlay * this.sampleSize) {

					// Ve con tro chay
					float pointer_X = marker1X + read * view.stepX
							/ this.sampleSize;

					Message msg = mHandler.obtainMessage();
					Bundle msgData = new Bundle();
					msgData.putFloat("pointer_X", pointer_X);
					msg.setData(msgData);
					mHandler.sendMessage(msg);

					at.write(music, 0, i);
					read += i;
				}

			} else {
				Toast.makeText(view.getContext(), "Khoảng quá nhỏ",
						Toast.LENGTH_LONG).show();
			}

			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isPlaying = false;
		at.stop();
		at.release();
	}

}
