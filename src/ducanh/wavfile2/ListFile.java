package ducanh.wavfile2;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ducanh.wavfile2.configs.WavFile2Config;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListFile extends Activity {

	ListView listFileView;
	List<String> listFileName;
	ArrayAdapter<String> adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.file_browser);
		listFileView = (ListView) findViewById(R.id.list_file);
		listFileName = new ArrayList<String>();
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listFileName);
		listFileView.setAdapter(adapter);
		listFileView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String fileName = adapter.getItem(position);
				Intent fileInfo = new Intent(getApplicationContext(), FileInfo.class);
				fileInfo.putExtra("fileName", getFileDir()+"/"+fileName);
				startActivity(fileInfo);
				finish();
			}
		});
		String directoryName = getFileDir();
		listf(directoryName, adapter);
	}

	public void listf(String directoryName, ArrayAdapter<String> files) {
		File directory = new File(directoryName);

		// get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				files.add(file.getName());
			} else if (file.isDirectory()) {
				listf(file.getAbsolutePath(), files);
			}
		}
	}
	
	private String getFileDir() {
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File fileDir = new File(filepath, WavFile2Config.AUDIO_RECORDER_FOLDER);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		return (fileDir.getAbsolutePath());
	}

}
