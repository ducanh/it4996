package ducanh.wavfile2;

import ducanh.wavfile2.trackview.TrackViewState;
import ducanh.wavfile2.trackview.TrackViewStateView;
import ducanh.wavfile2.trackview.WavTrackView;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ViewTTQ extends Activity {
	
	String fileName;
	
	LinearLayout drawFile, drawTTQ, drawTTQPoint;
	WavTrackView drawFileView;
	WavTrackView drawTTQView;
	WavTrackView drawTTQPointView;
	
	TrackViewState drawViewState;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_ttq);
		
		drawFile = (LinearLayout) findViewById(R.id.drawFile);
		drawFile.setBackgroundColor(Color.BLACK);
		
		drawTTQ = (LinearLayout) findViewById(R.id.drawTTQ);
		drawTTQ.setBackgroundColor(Color.BLACK);
		
		drawTTQPoint = (LinearLayout) findViewById(R.id.drawTTQPoint);
		drawTTQPoint.setBackgroundColor(Color.BLACK);
		
		String fileName = getIntent().getStringExtra("fileName");
		this.fileName = fileName;
		
		drawFileView = new WavTrackView(this);
		drawViewState = new TrackViewStateView(drawFileView, fileName);
		drawFileView.setState(drawViewState);
		drawFile.addView(drawFileView);
		
		drawTTQView = new WavTrackView(this);
		drawViewState = new TrackViewStateView(drawTTQView, fileName);
		drawTTQView.setState(drawViewState);
		drawTTQ.addView(drawTTQView);
		
		drawTTQPointView = new WavTrackView(this);
		drawViewState = new TrackViewStateView(drawTTQPointView, fileName);
		drawTTQPointView.setState(drawViewState);
		drawTTQPoint.addView(drawTTQPointView);
	}

}
