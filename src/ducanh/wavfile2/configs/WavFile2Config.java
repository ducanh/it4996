package ducanh.wavfile2.configs;

import android.media.AudioFormat;

public class WavFile2Config {
	public static final String AUDIO_RECORDER_FOLDER = "WavFile2Recorder";
	public static final String TAG = "<< WavRecording >>";
	public static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
	public static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
	
	public static final int RECORDER_BITSPERSAMPLE = 16;
	public static final int RECORDER_SAMPLERATE = 16000;
	public static final int NUMBER_CHANELS = 1;
	public static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	public static final int CHANNELS_CONFIG = AudioFormat.CHANNEL_CONFIGURATION_MONO;
	public static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
}
