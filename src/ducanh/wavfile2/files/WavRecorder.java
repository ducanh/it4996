package ducanh.wavfile2.files;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.PublicKey;

import ducanh.wavfile2.configs.WavFile2Config;
import ducanh.wavfile2.trackview.WavTrackView;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class WavRecorder {

	private AudioRecord recorder = null;
	private int bufferSize = 0;
	private Thread recordingThread;
	private boolean isRecording = false;
	private OnRecordingListener mOnRecordingListener = null;
	private Handler handler;
	private String newFileName = null;

	public WavRecorder() {
		bufferSize = AudioRecord.getMinBufferSize(
				WavFile2Config.RECORDER_SAMPLERATE,
				WavFile2Config.CHANNELS_CONFIG, AudioFormat.ENCODING_PCM_16BIT);
		
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				// làm gì đó với msg, có thể lấy dữ liệu bằng msg.getData()
				byte[] data = msg.getData().getByteArray("recordData");
				
				if (mOnRecordingListener != null) {
					mOnRecordingListener.onRecording(data);
				}
			}
		};
	}

	public boolean isRecording() {
		return this.isRecording;
	}

	public void startRecording() {
		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
				WavFile2Config.RECORDER_SAMPLERATE,
				WavFile2Config.RECORDER_CHANNELS,
				WavFile2Config.RECORDER_AUDIO_ENCODING, bufferSize);

		int i = recorder.getState();
		if (i == AudioRecord.STATE_INITIALIZED) {
			recorder.startRecording();
		}
		isRecording = true;
		recordingThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					writeAudioDataToFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}, "AudioRecorder Thread");

		recordingThread.start();
	}

	/**
	 * Ghi data ra temp file
	 * 
	 * @throws IOException
	 */
	private void writeAudioDataToFile() throws IOException {
		byte data[] = new byte[bufferSize];

		String filename = FileUtils.getTempFileName(
				WavFile2Config.AUDIO_RECORDER_FOLDER,
				WavFile2Config.AUDIO_RECORDER_TEMP_FILE);

		FileOutputStream os = new FileOutputStream(filename);
		int read = 0;

		if (os != null) {
			while (isRecording) {
				read = recorder.read(data, 0, bufferSize);
				if (read != AudioRecord.ERROR_INVALID_OPERATION) {
					os.write(data);
					
					Message msg = handler.obtainMessage();
					Bundle msgData = new Bundle();
					msgData.putByteArray("recordData", data);
					msg.setData(msgData);
					handler.sendMessage(msg);
				}
			}
			os.close();
		}
	}

	public void stopRecording() {
		if (recorder != null) {
			isRecording = false;

			int i = recorder.getState();
			if (i == AudioRecord.STATE_INITIALIZED) {
				recorder.stop();
			}
			recorder.release();
			recorder = null;
			recordingThread = null;

			String tempFileName = FileUtils.getTempFileName(
					WavFile2Config.AUDIO_RECORDER_FOLDER,
					WavFile2Config.AUDIO_RECORDER_TEMP_FILE);

			String newFileName = FileUtils.getFileName(
					WavFile2Config.AUDIO_RECORDER_FOLDER,
					WavFile2Config.AUDIO_RECORDER_FILE_EXT_WAV);

			this.copyWaveFile(tempFileName, newFileName);
			this.newFileName = newFileName;
			FileUtils.deleteFile(tempFileName);
		}
	}
	
	public String getNewFileName(){
		return this.newFileName;
	}

	protected void copyWaveFile(String inFileName, String outFileName) {
		FileInputStream in = null;
		FileOutputStream out = null;
		long totalAudioLen = 0;
		long totalDataLen = totalAudioLen + 36;
		long byteRate = WavFile2Config.RECORDER_BITSPERSAMPLE
				* WavFile2Config.RECORDER_SAMPLERATE
				* WavFile2Config.NUMBER_CHANELS / 8;

		byte[] data = new byte[bufferSize];

		try {
			in = new FileInputStream(inFileName);
			out = new FileOutputStream(outFileName);
			totalAudioLen = in.getChannel().size();
			totalDataLen = totalAudioLen + 36;

			WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
					WavFile2Config.RECORDER_SAMPLERATE,
					WavFile2Config.NUMBER_CHANELS, byteRate);

			while (in.read(data) != -1) {
				out.write(data);
			}

			in.close();
			out.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write wav file header
	 * 
	 * @param out
	 * @param totalAudioLen
	 * @param totalDataLen
	 * @param longSampleRate
	 * @param chanels
	 * @param byteRate
	 * @throws IOException
	 */
	private void WriteWaveFileHeader(FileOutputStream out, long totalAudioLen,
			long totalDataLen, long longSampleRate, int chanels, long byteRate)
			throws IOException {
		byte[] header = new byte[44];

		header[0] = 'R'; // RIFF/WAVE header
		header[1] = 'I';
		header[2] = 'F';
		header[3] = 'F';
		header[4] = (byte) (totalDataLen & 0xff);
		header[5] = (byte) ((totalDataLen >> 8) & 0xff);
		header[6] = (byte) ((totalDataLen >> 16) & 0xff);
		header[7] = (byte) ((totalDataLen >> 24) & 0xff);
		header[8] = 'W';
		header[9] = 'A';
		header[10] = 'V';
		header[11] = 'E';
		header[12] = 'f'; // 'fmt ' chunk
		header[13] = 'm';
		header[14] = 't';
		header[15] = ' ';
		header[16] = 16; // 4 bytes: size of 'fmt ' chunk
		header[17] = 0;
		header[18] = 0;
		header[19] = 0;
		header[20] = 1; // format = 1
		header[21] = 0;
		header[22] = (byte) chanels;
		header[23] = 0;
		header[24] = (byte) (longSampleRate & 0xff);
		header[25] = (byte) ((longSampleRate >> 8) & 0xff);
		header[26] = (byte) ((longSampleRate >> 16) & 0xff);
		header[27] = (byte) ((longSampleRate >> 24) & 0xff);
		header[28] = (byte) (byteRate & 0xff);
		header[29] = (byte) ((byteRate >> 8) & 0xff);
		header[30] = (byte) ((byteRate >> 16) & 0xff);
		header[31] = (byte) ((byteRate >> 24) & 0xff);
		header[32] = (byte) (1 * WavFile2Config.RECORDER_BITSPERSAMPLE / 8); // block
																				// align
		header[33] = 0;
		header[34] = WavFile2Config.RECORDER_BITSPERSAMPLE; // bits per sample
		header[35] = 0;
		header[36] = 'd';
		header[37] = 'a';
		header[38] = 't';
		header[39] = 'a';
		header[40] = (byte) (totalAudioLen & 0xff);
		header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
		header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
		header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

		out.write(header, 0, 44);
	}

	public interface OnRecordingListener {
		public void onRecording(byte[] data);
	}

	public void setOnRecordingListener(OnRecordingListener l) {
		this.mOnRecordingListener = l;
	}
}
