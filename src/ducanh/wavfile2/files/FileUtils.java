package ducanh.wavfile2.files;

import java.io.File;

import android.os.Environment;

public class FileUtils {
	/**
	 * Get one file name
	 * @param directory
	 * @param extension
	 * @return file name by time millis
	 */
	public static String getFileName(String directory, String extension) {
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File fileDir = new File(filepath, directory);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}
		return (fileDir.getAbsolutePath() + "/" + System.currentTimeMillis() + extension);
	}
	
	/**
	 * Get temp file name
	 * Neu file da ton tai thi se bi xoa
	 * @param directory
	 * @param fileNameWithExtension
	 * @return file name
	 */
	public static String getTempFileName(String directory, String fileNameWithExtension) {
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File fileDir = new File(filepath, directory);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		File tempFile = new File(filepath, fileNameWithExtension);

		if (tempFile.exists()) {
			tempFile.delete();
		}

		return (fileDir.getAbsolutePath() + "/" + fileNameWithExtension);
	}
	
	
	/**
	 * Xoa file, full file name
	 * @param filename
	 */
	public static void deleteFile(String filename) {
		File file = new File(filename);
		if(file.exists()){
			file.delete();
		}
	}
}
