package ducanh.wavfile2.files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class WavReader {

	private FileInputStream fis;
	private int pkglen, channels, sampleRate, sampleSize, // Bytes per sample
			numSamples;
	private List<Integer> drawData; // data de hien thi du lieu
	List<byte[]> playData; // data de play du lieu

	public WavReader(FileInputStream thefis) {
		fis = thefis;
		boolean ok = readHeader();
		if (!ok) {
			System.err.println("bad header!");
		}
		this.drawData = new  ArrayList<Integer>();
		this.playData = new ArrayList<byte[]>();
		
	}

	public WavReader(File f) {
		long length = f.length();
		try {
			fis = new FileInputStream(f);
		} catch (FileNotFoundException fnfe) {
			System.err.println("WavReader cannot open file " + f);
			channels = sampleRate = sampleSize = numSamples = 0;
			return;
		}
		readHeader();
		int computedSampleCount = (int) (length - 44) / sampleSize;
		if (computedSampleCount != numSamples) {
			System.err.println("numSample disagreement: from header: "
					+ numSamples + ", from file size: " + computedSampleCount);
			numSamples = computedSampleCount;
		}
		this.drawData = new  ArrayList<Integer>();
		this.playData = new ArrayList<byte[]>();
	}

	public int getNumberSamples() {
		return this.numSamples;
	}
	
	public int getNumberChanels(){
		return this.channels;
	}

	// return sapmle size (bytes)
	public int getSampleSize() {
		return this.sampleSize;
	}

	public int getSampleRate() {
		return this.sampleRate;
	}

	private void makeDrawData() {
		try {
			for (int i = 0; i < numSamples; i++) {
				int data = this.readDataShort(fis);
				this.drawData.add(Integer.valueOf(data));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Integer> getDrawData(int offset, int number) {
		if(this.drawData.size() == 0){
			this.makeDrawData();
		}
		
		if(offset > 0 && number > 0){
			int end = offset+number;
			if(end > this.drawData.size()){
				end = this.drawData.size();
			}
			List<Integer> ret = new ArrayList<Integer>();
			for(int i = offset; i < end; i++){
				ret.add(this.drawData.get(i));
			}
			return ret;
		}
		
		return this.drawData;
	}
	
	public FileInputStream getInputStream(){
		return this.fis;
	}
	
	public List<byte[]> getPlayData() {
		if(this.drawData.size() == 0){
			this.makeDrawData();
		}
		return this.playData;
	}

	private boolean readHeader() {
		try {
			String s = readString(fis); // 0-3
			msg_assert(s.equals("RIFF"), "missing \"RIFF\" tag in RIFF chunk");

			pkglen = readLEint(fis); // 4-7
			System.err.println("Pkglen = " + pkglen);

			s = readString(fis); // 8-11
			msg_assert(s.equals("WAVE"), "missing \"WAVE\" tag in RIFF chunk");

			s = readString(fis); // 12-15
			msg_assert(s.equals("fmt "),
					"missing \"fmt_\" tag in FORMAT chunk; found \"" + s + "\"");

			int pcm = readLEint(fis); // 16-19
			msg_assert(pcm == 0x10, "unknown length of FORMAT chunk: " + pcm);

			int lin = readLEshort(fis); // 20-21
			msg_assert(lin == 1, "code for nonlinear quantization: " + lin);

			channels = readLEshort(fis); // 22-23
			System.err.println("number of channels = " + channels);

			sampleRate = readLEint(fis); // 24-27
			System.err.println("sample rate: " + sampleRate);

			int Bpsec = readLEint(fis); // 28-31

			sampleSize = readLEshort(fis);// 32-33
			System.err.println("Bytes per sample: " + sampleSize);

			msg_assert(Bpsec == sampleRate * sampleSize,
					"inconsistent Bytes/sec: given: " + Bpsec + ", computed: "
							+ sampleRate * sampleSize);

			int bitspersample = readLEshort(fis);
			msg_assert(bitspersample == 8 * sampleSize / channels,
					"bits/sample given as " + bitspersample
							+ " but computed as " + 8 * sampleSize / channels);

			s = readString(fis);
			msg_assert(s.equals("data"),
					"missing \"data\" tag in DATA chunk; found \"" + s + "\"");

			int size = readLEint(fis);
			numSamples = size / channels / sampleSize;
			System.err.println("Number of samples according to header: "
					+ numSamples);
			return true;
		} catch (IOException ioe) {
			System.err.println("Problem with header format!");
			return false;
		}

	}

	private static void msg_assert(boolean cond, String message)
			throws IOException {
		if (!cond) {
			System.err.println(message);
			throw new IOException();
		}
	}

	/*
	 * reads a 4-byte string from the file and returns it
	 */

	public static String readString(InputStream fis) throws IOException {
		byte[] buf = new byte[4];
		int bytesread = fis.read(buf, 0, 4);
		if (bytesread != 4)
			throw new IOException();
		return new String(buf);
	}

	/*
	 * reads a 4-byte little-endian integer from the file and returns it
	 */
	public static int readLEint(InputStream fis) throws IOException {
		byte[] buf = new byte[4];
		int bytesread = fis.read(buf, 0, 4);
		if (bytesread != 4)
			throw new IOException();
		return (((buf[3] & 0xff) << 24) | ((buf[2] & 0xff) << 16)
				| ((buf[1] & 0xff) << 8) | ((buf[0] & 0xff)));
	}

	/*
	 * reads a 2-byte little-endian integer from the file and returns it
	 */
	public static int readLEshort(InputStream fis) throws IOException {
		byte[] buf = new byte[2];
		int bytesread = fis.read(buf, 0, 2);
		if (bytesread != 2)
			throw new IOException();
		return (((buf[1] & 0xff) << 8) | ((buf[0] & 0xff)));
	}

	/**
	 * @param fis
	 * @return so nguyen co dau 2 bytes
	 * @throws IOException
	 */
	public int readDataShort(InputStream fis) throws IOException {
		byte[] buf = new byte[2];
		int bytesread = fis.read(buf, 0, 2);
		if (bytesread != 2)
			throw new IOException();
		this.playData.add(buf);
		return (short) (((buf[1] & 0xff) << 8) | ((buf[0] & 0xff)));
	}
}
